<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>JWT PHP Front Temp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link" href="#" id='home'>Home</a>
          <a class="nav-item nav-link" href="#" id='update_account'>Account</a>
          <a class="nav-item nav-link" href="#" id='logout'>Logout</a>
          <a class="nav-item nav-link" href="#" id='login'>Login</a>
          <a class="nav-item nav-link" href="#" id='sign_up'>Sign Up</a>
        </div>
      </div>
    </nav>
    <main role="main" class="container starter-template">
      <div class="row">
        <div class="col">
          <div id="response"></div>
          <div id="content"></div>
        </div>
      </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="public/js/main.js"></script>
  </body>
</html>
