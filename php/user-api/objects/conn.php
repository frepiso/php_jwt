<?php
defined('ABSINTEGRITY_ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q') or die("Unauthorized");

class Conn {
    private $host = CONN_DB_HOST;
    private $db_name = CONN_DB_NAME;
    private $username = CONN_DB_USER;
    private $password = CONN_DB_PASS;
    public $conn;

    public function getConnection(){
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        } catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}
?>
