<?php
defined('ABSINTEGRITY_ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q') or die("Unauthorized");
//Connection
define('CONN_DB_HOST', 'localhost');
define('CONN_DB_NAME', 'devdb');
define('CONN_DB_USER', 'devuser');
define('CONN_DB_PASS', 'devpass');
//Endpoints
define('ENDPOINT_ORIGIN', 'http://localhost/user-api/')
define('ENDPOINT_CREATE_USER', ENDPOINT_ORIGIN . 'api/create_user.php');
define('ENDPOINT_UPDATE_USER', ENDPOINT_ORIGIN. 'api/update_user.php');
define('ENDPOINT_LOGIN_USER', ENDPOINT_ORIGIN. 'api/login.php');
define('ENDPOINT_VALIDATE_TOKEN', ENDPOINT_ORIGIN. 'api/validate_token.php');
?>
