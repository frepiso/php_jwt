<?php
defined('ABSINTEGRITY_ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q') or die("Unauthorized");
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

define('JWT_KEY', 'example_key');
define('JWT_ISS', 'http://example.org');
define('JWT_AUD', 'http://example.org');
define('JWT_IAT', '1356999524');
define('JWT_NBF', '1357000000');
?>
