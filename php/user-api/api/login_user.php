<?php
require_once('user-service.php');
header("Access-Control-Allow-Origin: ". ENDPOINT_ORIGIN . "");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// required to decode jwt
require_once 'config/config_jwt.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

$data = json_decode(file_get_contents("php://input"));
$userService = userService();
$userService->user->email = $data->email;
$email_exists = $userService->user->emailExists();

if($email_exists && password_verify($data->password, $userService->user->password)){
    $token = array(
       "iss" => JWT_ISS,
       "aud" => JWT_AUD,
       "iat" => JWT_IAT,
       "nbf" => JWT_NBF,
       "data" => array(
         "id" => $userService->user->id,
         "firstname" => $userService->user->firstname,
         "lastname" => $userService->user->lastname,
         "email" => $userService->user->email
       )
    );
    http_response_code(200);
    $jwt = JWT::encode($token, JWT_KEY);
    echo json_encode(array(
      "message" => "Successful login.",
      "jwt" => $jwt
    ));
} else {
    http_response_code(401);
    echo json_encode(array("message" => "Login failed."));
}
?>
