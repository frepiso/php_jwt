<?php
require_once('user-service.php');
header("Access-Control-Allow-Origin: ". ENDPOINT_ORIGIN . "");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// required to decode jwt
require_once 'config/config_jwt.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

$data = json_decode(file_get_contents("php://input"));
$userService = userService();

$jwt = isset($data->jwt) ? $data->jwt : "";
if($jwt) {
    try {
        $decoded = JWT::decode($jwt, JWT_KEY, array('HS256'));

        $userService->user->firstname = $data->firstname;
        $userService->user->lastname = $data->lastname;
        $userService->user->email = $data->email;
        $userService->user->password = $data->password;
        $userService->user->id = $decoded->data->id;

        if ($userService->user->update()) {

          $token = array(
            "iss" => JWT_ISS,
            "aud" => JWT_AUD,
            "iat" => JWT_IAT,
            "nbf" => JWT_NBF,
            "data" => array(
               "id" => $userService->user->id,
               "firstname" => $userService->user->firstname,
               "lastname" => $userService->user->lastname,
               "email" => $userService->user->email
             )
          );
          $jwt = JWT::encode($token, JWT_KEY);

          http_response_code(200);
          echo json_encode(array(
              "message" => "User was updated.",
              "jwt" => $jwt
          ));
        } else {
            http_response_code(401);
            echo json_encode(array("message" => "Unable to update user."));
        }
    } catch (Exception $e) {
      http_response_code(401);
      echo json_encode(array(
        "message" => "Access denied.",
        "error" => $e->getMessage()
      ));
  }
} else {
    http_response_code(401);
    echo json_encode(array("message" => "Access denied."));
}
?>
