<?php
require_once('user-service.php');
header("Access-Control-Allow-Origin: ". ENDPOINT_ORIGIN . "");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$data = json_decode(file_get_contents("php://input"));

$userService = userService();
$userService->user->firstname = $data->firstname;
$userService->user->lastname = $data->lastname;
$userService->user->email = $data->email;
$userService->user->password = $data->password;

if(!empty($userService->user->firstname) &&
  !empty($userService->user->email) &&
  !empty($userService->user->password) &&
  $$userService->user->create()) {
    http_response_code(200);
    echo json_encode(array("message" => "User was created."));
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create user."));
}
?>
