<?php
if (file_exists("install.php") || class_exists('UserService')) {
  exit;
} else {
  final class UserService {

    private static $instance;
    public $conn;
    public $db;
    public $user;

    public static function instance() {
      	if (!isset(self::$instance ) && !(self::$instance instanceof UserService)) {
          self::$instance = new UserService;
          self::$instance->definitions();
          self::$instance->includes();
          self::$instance->start();
          add_action('plugins_loaded', array(self::$instance, 'objects'), 10);
        }
        return self::$instance;
    }

    private function definitions() {
      define('ABSINTEGRITY_ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q');

      if (!defined( 'DIR_PATH')) {
			     define('DIR_PATH', dir_path(__FILE__));
			}
    }

    private function includes() {
      require_once DIR_PATH . 'config/config.php';
      require_once DIR_PATH . 'objects/conn.php';
      require_once DIR_PATH . 'objects/user.php';
    }

    private function start() {
      $this->conn = new Conn();
      $this->db = $this->conn->getConnection();
      $this->user = new User($this->db);
    }
  }
}

function userService() {
  return UserService::instance();
}
?>
