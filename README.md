# Getting started
## Initial setup and installation

How to Start
-
```
docker-compose up -d
```

Stop:
```
docker-compose stop
```

Load http://localhost:8000/ in your browser to check the API
